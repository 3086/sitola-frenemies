
docker run -v $PWD:/work -w /work -ti ubuntu 
apt update
apt install python3-pip	# -> :1

docker run -v $PWD:/work -w /work -ti ljocha/frenemies:1
pip3 install notebook

docker run -ti -v $PWD:/work -p 8000:8000 -u $(id -u) -e HOME=/work -w /work ljocha/frenemies:2 jupyter notebook --port 8000 --ip 0.0.0.0

pip3 install matplotlib # -> :3



docker build -t ljocha/rabbits -f Dockerfile.rabbits .


docker exec 375a3aaaf18c jupyter nbconvert --to notebook rabbits.ipynb --execute




Baker, M., 2016. 1,500 scientists lift the lid on reproducibility. Nature News, 533(7604), p.452.
